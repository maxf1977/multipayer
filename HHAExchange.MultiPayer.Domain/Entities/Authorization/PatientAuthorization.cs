﻿using System;

namespace HHAExchange.MultiPayer.Domain.Entities.Authorization
{
    public class PatientAuthorization
    {
        public int PatientAuthorizationID { get; set; }

        public string AuthNumber { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public DateTime FromDateOrder { get; set; }

        public DateTime ToDateOrder { get; set; }

        public int Period { get; set; }

        public int ServiceCategoryID { get; set; }

        public string ServiceCategory { get; set; }

        public int ServiceTypeID { get; set; }

        public string ServiceType { get; set; }

        public string VendorID { get; set; }

        public int ServiceCodeID { get; set; }

        public string ServiceCode { get; set; }

        public string ServiceCodeType { get; set; }

        public string AuthorizationType { get; set; }

        public decimal Maximum { get; set; }

        public int TotalHours { get; set; }

        public string SatStartTimeType { get; set; }

        public string SatFromTime { get; set; }

        public string SatToTime { get; set; }

        public string SunStartTimeType { get; set; }

        public string SunFromTime { get; set; }

        public string SunToTime { get; set; }

        public string MonStartTimeType { get; set; }

        public string MonFromTime { get; set; }

        public string MonToTime { get; set; }

        public string TueStartTimeType { get; set; }

        public string TueFromTime { get; set; }

        public string TueToTime { get; set; }

        public string WedStartTimeType { get; set; }

        public string WedFromTime { get; set; }

        public string WedToTime { get; set; }

        public string ThuStartTimeType { get; set; }

        public string ThuFromTime { get; set; }

        public string ThuToTime { get; set; }

        public string FriStartTimeType { get; set; }

        public string FriFromTime { get; set; }

        public string FriToTime { get; set; }

        public decimal SatHours { get; set; }

        public decimal SunHours { get; set; }

        public decimal MonHours { get; set; }

        public decimal TueHours { get; set; }

        public decimal WedHours { get; set; }

        public decimal ThuHours { get; set; }

        public decimal FriHours { get; set; }

        public string Provider { get; set; }

        public string Payer { get; set; }

        public string Notes { get; set; }

        public string ScanFileName { get; set; }

        public string ScanFileGUID { get; set; }

        public int PatientID { get; set; }

        public long PlacementID { get; set; }

        public int StatusID { get; set; }

        public string ChhaID { get; set; }

        public int IsAssignedToVisits { get; set; }

        public bool AdditionalRules { get; set; }

        public string AdditionalRulesText { get; set; }

        public int? BlackOutDates { get; set; }

        public string InternalNotes { get; set; }

        public string DX1 { get; set; }

        public string DX2 { get; set; }

        public string DX3 { get; set; }

        public string HomeCare { get; set; }

        public string InvoiceLimit { get; set; }

        public string UserName { get; set; }

        public string PayerName { get; set; }
    }
}
