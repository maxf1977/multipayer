﻿namespace HHAExchange.MultiPayer.Domain.Entities.Payer
{
    public class AgencyKeyword
    {
        public int Id { get; set; }

        public int PayerId { get; set; }

        public string PayerType { get; set; }

        public string Keyword { get; set; }

        public string AlternativeKeyword { get; set; }
    }
}
