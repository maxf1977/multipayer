﻿using System;

namespace HHAExchange.API.Extensions.Authorization
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ClaimUserRoleAttribute : Attribute
    {
        public ClaimUserRoleAttribute(string claimRoleName) => Name = claimRoleName;

        public string Name { get; }
    }
}
