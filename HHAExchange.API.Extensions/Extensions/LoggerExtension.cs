﻿using Microsoft.Extensions.Hosting;
using Serilog;

namespace HHAExchange.MultiPayer.API.Extensions
{
    public static class LoggerExtension
    {
        public static IHostBuilder ConfigureLogger<TLogHost>(this IHostBuilder hostBuilder)
        {
            hostBuilder.UseSerilog((hostingContext, loggerConfiguration) =>
             {
                 loggerConfiguration
                 .Enrich.WithProperty("ApplicationContext", GetAppName<TLogHost>())
                 .ReadFrom.Configuration(hostingContext.Configuration);
             });

            return hostBuilder;
        }

        private static string GetLogNamespace<TLogHost>() => typeof(TLogHost).Namespace;

        private static string GetAppName<TLogHost>()
        {
             var nspace = GetLogNamespace<TLogHost>();
             return nspace.Substring(nspace.LastIndexOf('.', nspace.LastIndexOf('.') - 1) + 1);
        }
    }
}
