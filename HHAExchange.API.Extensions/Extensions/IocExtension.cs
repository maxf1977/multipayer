﻿using System;
using System.Linq;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace HHAExchange.MultiPayer.API.Extensions
{
    public static class IocExtension
    {
        public static IHostBuilder ConfigureIoc(this IHostBuilder builder) => builder.UseServiceProviderFactory(new AutofacServiceProviderFactory());

        public static ContainerBuilder RegisterModules(this ContainerBuilder builder, IConfiguration configuration, params Type[] moduleTypes)
        {
            if (moduleTypes.Any())
            {
                foreach (var moduleType in moduleTypes)
                {
                    if (!moduleType.IsAssignableTo<Module>())
                    {
                        throw new ArgumentException(moduleType.FullName);
                    }

                    builder.RegisterModule((Module)Activator.CreateInstance(moduleType, configuration));
                }
            }

            return builder;
        }
    }
}
