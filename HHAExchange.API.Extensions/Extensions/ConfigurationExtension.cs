﻿using System;
using System.IO;
using HHAExchange.API.Extensions.Extensions;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace HHAExchange.API.Extensions.Extensions
{
    public static class ConfigurationExtension
    {
        public static IHostBuilder ConfigureAppConfiguration(
            this IHostBuilder hostBuilder,
            params Func<IConfigurationBuilder, IConfigurationBuilder>[] configureFuncs)
        {
            hostBuilder.ConfigureAppConfiguration(x =>
            {
                var configurationBuilder = CreateConfiguration();
                foreach (var configurationFunc in configureFuncs)
                {
                    configurationBuilder = configurationFunc(configurationBuilder);
                }

                x.AddConfiguration(configurationBuilder.Build());
            });

            return hostBuilder;
        }

        public static IConfigurationBuilder AddAppSettingsConfiguration(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true);

            return configurationBuilder;
        }

        public static IConfigurationBuilder AddStoredProceduresConfiguration(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.AddJsonFile("storedProcedures.json", optional: false, reloadOnChange: true);
            return configurationBuilder;
        }

        public static IConfigurationBuilder AddCustomConfiguration(this IConfigurationBuilder configurationBuilder, string path, bool optional, bool reloadOnChange)
        {
            configurationBuilder.AddJsonFile(path, optional, reloadOnChange);
            return configurationBuilder;
        }

        public static IConfigurationBuilder CreateConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables();
        }
    }
}
