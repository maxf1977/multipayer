﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using HHAExchange.API.Extensions.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace HHAExchange.MultiPayer.API.Extensions
{
    public static class SecurityExtension
    {
        private const string HHAORIGINS = "_hhaOrigins";

        public static IServiceCollection AddAuthenticationServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(OpenIdConnectDefaults.AuthenticationScheme)
                    .AddOpenIdConnect(options =>
                    {
                        options.ForwardDefaultSelector = c => JwtBearerDefaults.AuthenticationScheme;

                        options.Authority = configuration["Identity:Authority"];
                        options.ClientId = configuration["Identity:ClientId"];
                        options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                        options.SignOutScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    })
                    .AddCookie()
                    .AddJwtBearer("Bearer", options =>
                    {
                        var tokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new X509SecurityKey(new X509Certificate2(Path.Combine(configuration["Certificate:Path"], configuration["Certificate:Name"]), configuration["Certificate:Password"])),
                            ValidateIssuer = false,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidAudiences = new List<string>
                            {
                                "Payer"
                            },
                            ClockSkew = TimeSpan.Zero
                        };

                        options.Authority = configuration["Identity:Authority"];
                        options.RequireHttpsMetadata = false;
                        options.Audience = "PayerApi";
                        options.TokenValidationParameters = tokenValidationParameters;
                        options.Events = new JwtBearerEvents
                        {
                            OnMessageReceived = context =>
                            {
                                var accessToken = context.Request.Query["access_token"];
                                var path = context.HttpContext.Request.Path;

                                context.Token = accessToken;

                                return Task.CompletedTask;
                            }
                        };
                    });

            return services;
        }

        public static IServiceCollection AddAuthorizationPolicies<TPolicyEnum>(this IServiceCollection services)
            where TPolicyEnum : struct, IConvertible
        {
            if (!typeof(TPolicyEnum).IsEnum)
            {
                throw new ArgumentException(nameof(TPolicyEnum));
            }

            services.AddAuthorization(options =>
            {
                foreach (TPolicyEnum enumValue in Enum.GetValues(typeof(TPolicyEnum)))
                {
                    if (TryGetClaimUserRole(enumValue, out string claimUserRole))
                    {
                        options.AddPolicy(enumValue.ToString(), policy => policy.RequireClaim(claimUserRole));
                    }
                }
            });

            return services;
        }

        public static IServiceCollection AddDataProtectionServices(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentException(nameof(configuration));
            }

            string keyStorePath = configuration["Certificate:Path"];
            if (string.IsNullOrEmpty(keyStorePath) || !Directory.Exists(keyStorePath))
            {
                throw new Exception("Invalid Keystore Path provided");
            }

            services.AddDataProtection()
                    .SetApplicationName(configuration["Certificate:IdentityServerName"])
                    .PersistKeysToFileSystem(new DirectoryInfo(keyStorePath))
                    .ProtectKeysWithDpapi();

            return services;
        }

        public static IApplicationBuilder UseHHACors(this IApplicationBuilder application)
        {
            application.UseCors(HHAORIGINS);
            return application;
        }

        public static IServiceCollection AddHHACors(this IServiceCollection services)
        {
            services.AddCors(options =>
                {
                    options.AddPolicy(
                        HHAORIGINS,
                        builder =>
                        {
                            builder.WithOrigins("http://localhost:4200")
                                    .AllowAnyHeader()
                                    .AllowAnyMethod()
                                    .AllowAnyOrigin();
                        });
                });

            return services;
        }

        private static bool TryGetClaimUserRole<TPolicyEnum>(TPolicyEnum enumValue, out string claimUserRole)
        {
            claimUserRole = null;
            var enumMembers = typeof(TPolicyEnum).GetMember(enumValue.ToString());
            if (enumMembers != null && enumMembers.Any())
            {
                var claimUserRoleAttribute = enumMembers[0].GetCustomAttributes(typeof(ClaimUserRoleAttribute), false).FirstOrDefault();
                if (claimUserRoleAttribute != null)
                {
                    claimUserRole = ((ClaimUserRoleAttribute)claimUserRoleAttribute).Name;
                    return true;
                }
            }

            return false;
        }
    }
}
