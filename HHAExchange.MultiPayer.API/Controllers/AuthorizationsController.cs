﻿using System;
using System.Net;
using System.Threading.Tasks;

using HHAExchange.MultiPayer.Application.Authorizations;

using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace HHAExchange.MultiPayer.API.Controllers
{
    [Route("patients")]
    [ApiController]
    public class AuthorizationsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AuthorizationsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        [Route("{patientId}/authorizations/")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<ActionResult> GetPatientAuthorizations([FromQuery] GetPatientAuthorizationsRequestModel requestModel)
        {
            var authorizations = await _mediator.Send(requestModel);

            if (authorizations == null)
            {
                return NoContent();
            }

            return Ok(authorizations);
        }
    }
}
