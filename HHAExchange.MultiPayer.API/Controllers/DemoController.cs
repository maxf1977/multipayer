﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HHAExchange.MultiPayer.Application.Payer;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HHAExchange.MultiPayer.API.Controllers
{
    [AllowAnonymous]
    [Route("[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DemoController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public Task<ActionResult> Get()
        {
            return Ok("hello");
        }

        [HttpGet]
        [Route("/ak/{itemCount}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<ActionResult> GetPatientAuthorizations([FromQuery] GetAgencyKeywordsRequestModel requestModel)
        {
            var response = await _mediator.Send(requestModel);

            if (response == null)
            {
                return NoContent();
            }

            return Ok(response);
        }
    }
}
