using HHAExchange.API.Extensions.Extensions;
using HHAExchange.MultiPayer.API.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace HHAExchange.MultiPayer
{
    public sealed class Program
    {
        public static void Main(string[] args) => CreateHostBuilder(args).Build().Run();

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureIoc()
            .ConfigureAppConfiguration(
                ConfigurationExtension.AddAppSettingsConfiguration/*, ConfigurationExtension.AddStoredProceduresConfiguration*/)
            .ConfigureLogger<Program>()
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>()
                          .CaptureStartupErrors(true);
            });
    }
}
