﻿using HHAExchange.API.Extensions.Authorization;

namespace HHAExchange.MultiPayer.API.Authorization
{
    public enum UserPermissionRole
    {
        [ClaimUserRole("hha.payer.canaccesspayer")]
        CanAccessPayer,
        [ClaimUserRole("hha.payer.canviewhome")]
        CanViewHome,
        [ClaimUserRole("hha.payer.canviewmessagecenter")]
        CanViewMessageCenter,
        [ClaimUserRole("hha.payer.canviewpatientauthorizations")]
        CanViewPatientAuthorizations,
        [ClaimUserRole("hha.payer.canviewpatientcalendar")]
        CanViewPatientCalendar,
        [ClaimUserRole("hha.payer.canviewpatientgeneral")]
        CanViewPatientGeneral,
        [ClaimUserRole("hha.payer.canviewpatientprofile")]
        CanViewPatientProfile,
        [ClaimUserRole("hha.payer.canviewpatientsearch")]
        CanViewPatientSearch,
        [ClaimUserRole("hha.payer.canviewpatientvisits")]
        CanViewPatientVisits
    }
}
