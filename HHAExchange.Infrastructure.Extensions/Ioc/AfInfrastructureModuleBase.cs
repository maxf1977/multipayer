﻿using Autofac;
using HHAExchange.Extensions.Base.Ioc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace HHAExchange.Infrastructure.Extensions
{
    public abstract class AfInfrastructureModuleBase : AfModuleBase
    {

        public AfInfrastructureModuleBase(IConfiguration configuration) : base(configuration)
        {
        }

        public ContainerBuilder RegisterAsHhaDbContext<TContext>(ContainerBuilder containerBuilder)
            where TContext : DbContext
            => RegisterDbContext<TContext>(containerBuilder, Configuration["ConnectionString:hha"]);

        public ContainerBuilder RegisterAsHha2DbContext<TContext>(ContainerBuilder containerBuilder)
            where TContext : DbContext
            => RegisterDbContext<TContext>(containerBuilder, Configuration["ConnectionString:hha2"]);

        public ContainerBuilder RegisterAsPayerPhiDbContext<TContext>(ContainerBuilder containerBuilder)
            where TContext : DbContext
            => RegisterDbContext<TContext>(containerBuilder, Configuration["ConnectionString:payerPHI"]);

        public ContainerBuilder RegisterAsNonHomeCareContext<TContext>(ContainerBuilder containerBuilder)
            where TContext : DbContext
            => RegisterDbContext<TContext>(containerBuilder, Configuration["ConnectionString:NonHomeCare"]);

        public ContainerBuilder RegisterAsEBillingDbContext<TContext>(ContainerBuilder containerBuilder)
            where TContext : DbContext
            => RegisterDbContext<TContext>(containerBuilder, Configuration["ConnectionString:EBilling"]);

        public ContainerBuilder RegisterDbContext<TContext>(ContainerBuilder containerBuilder, string connectionString)
            where TContext : DbContext
        {
            containerBuilder.RegisterType<TContext>()
           .WithParameter(
               "options",
               new DbContextOptionsBuilder<TContext>().UseSqlServer(connectionString, sqlServerOptionsAction: sqlOptions =>
               {
                   sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
               }).Options)
           .InstancePerLifetimeScope();

            return containerBuilder;
        }
    }
}
