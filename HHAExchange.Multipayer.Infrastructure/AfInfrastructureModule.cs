﻿using System;
using Autofac;
using HHAExchange.Infrastructure.Extensions;
using HHAExchange.Payer.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace HHAExchange.Multipayer.Infrastructure
{
    public class AfInfrastructureModule : AfInfrastructureModuleBase
    {
        public AfInfrastructureModule(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void Load(ContainerBuilder containerBuilder)
        {
            RegisterAsHhaDbContext<HhaContext>(containerBuilder);
            RegisterAsHha2DbContext<Hha2Context>(containerBuilder);
            RegisterAsPayerPhiDbContext<PayerPHIContext>(containerBuilder);
            RegisterAsNonHomeCareContext<NonHomeCareContext>(containerBuilder);
            RegisterAsEBillingDbContext<EBillingContext>(containerBuilder);
        }      
    }
}
