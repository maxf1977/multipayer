﻿using HHAExchange.MultiPayer.Domain.Entities.Payer;
using HHAExchange.Payer.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HHAExchange.Multipayer.Infrastructure.Configurations
{
    public class AgencyKeywordsEntityTypeConfiguration : IEntityTypeConfiguration<AgencyKeyword>
    {
        public void Configure(EntityTypeBuilder<AgencyKeyword> agencyKeywordsConfiguration)
        {
            agencyKeywordsConfiguration.ToTable("AgencyKeywords", HhaContext.DEFAULTSCHEMA);
            agencyKeywordsConfiguration.HasKey(ak => ak.Id);
            agencyKeywordsConfiguration.Property(ak => ak.Id).HasColumnName("AgencyKeywordID");
            agencyKeywordsConfiguration.Property(ak => ak.PayerId).HasColumnName("AgencyID");
            agencyKeywordsConfiguration.Property(ak => ak.PayerType).HasColumnName("AgencyType");
        }
    }
}
