﻿using Microsoft.EntityFrameworkCore;

namespace HHAExchange.Payer.Infrastructure
{
    public class Hha2Context : DbContext
    {
        public Hha2Context(DbContextOptions<Hha2Context> options)
            : base(options)
        {
        }

        public const string DEFAULTSCHEMA = "dbo";

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}