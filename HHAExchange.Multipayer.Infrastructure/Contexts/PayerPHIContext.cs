using Microsoft.EntityFrameworkCore;

namespace HHAExchange.Payer.Infrastructure
{
    public class PayerPHIContext : DbContext
    {
        public PayerPHIContext(DbContextOptions<PayerPHIContext> options)
            : base(options)
        {
        }
    }
}
