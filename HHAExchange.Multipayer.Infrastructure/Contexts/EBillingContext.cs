﻿using Microsoft.EntityFrameworkCore;

namespace HHAExchange.Payer.Infrastructure
{
    public class EBillingContext : DbContext
    {
        public EBillingContext(DbContextOptions<EBillingContext> options)
            : base(options)
        {
        }

        public const string DEFAULTSCHEMA = "EBilling";

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}