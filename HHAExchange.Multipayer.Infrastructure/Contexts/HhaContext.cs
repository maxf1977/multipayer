﻿using HHAExchange.Multipayer.Infrastructure.Configurations;
using HHAExchange.MultiPayer.Domain.Entities.Payer;
using Microsoft.EntityFrameworkCore;

namespace HHAExchange.Payer.Infrastructure
{
    public class HhaContext : DbContext
    {
        public HhaContext(DbContextOptions<HhaContext> options)
            : base(options)
        {
        }

        public const string DEFAULTSCHEMA = "dbo";
        public const string PAYERSCHEMA = "Payer";

        public DbSet<AgencyKeyword> AgencyKeywords { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AgencyKeywordsEntityTypeConfiguration());
        }
    }
}