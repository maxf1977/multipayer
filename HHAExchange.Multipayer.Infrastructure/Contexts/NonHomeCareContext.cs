using Microsoft.EntityFrameworkCore;

namespace HHAExchange.Payer.Infrastructure
{
    public class NonHomeCareContext : DbContext
    {
        public NonHomeCareContext(DbContextOptions<NonHomeCareContext> options)
            : base(options)
        {
        }

        public const string DEFAULTSCHEMA = "NHC";

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
