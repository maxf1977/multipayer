#addin "nuget:?package=Cake.ArgumentHelpers"
#addin "Cake.Npm"
#addin "Cake.Karma"
#tool "nuget:?package=GitVersion.CommandLine&Version=4.0.0"
#tool "nuget:?package=NUnit.ConsoleRunner"
#tool "nuget:?package=NUnit.Extension.TeamCityEventListener"
#tool "nuget:?package=OctopusTools"
#tool "nuget:?package=TeamCity.VSTest.TestAdapter"
#module "nuget:?package=Cake.BuildSystems.Module"

var OctopusDeployUrl = "https://deploy.hhaexchange.com";

var Target = Argument("target", "Build-API");
var BuildNumber = ArgumentOrEnvironmentVariable("build.number", "", "0.0.1-local.0");
var OctopusDeployApiKey = ArgumentOrEnvironmentVariable("OctopusDeployApiKey", "");
var MsBuildLogger = ArgumentOrEnvironmentVariable("MsBuildLogger", "", "");
var ProjectName = ArgumentOrEnvironmentVariable("ProjectName","","Payer");
var DeploymentBranches = ArgumentOrEnvironmentVariable("DeploymentBranches", "", " ");
var TeamCityBuildAgentDirectory = ArgumentOrEnvironmentVariable("teamcity.agent.home.dir", "", "c:\\BuildAgent");

    Action<FilePath, ProcessArgumentBuilder> Cmd => (path, args) => {
        var result = StartProcess(
            path,
            new ProcessSettings {
                Arguments = args
            });

        if (0 != result)
        {
            throw new Exception($"Failed to execute tool {path.GetFilename()} ({result})");
        }
    };

string BranchName = null;
string tenant = null;

var ngPath = Context.Tools.Resolve("ng.cmd");
var npmPath = Context.Tools.Resolve("npm.cmd");

Task("Version")
    .Does(() =>
{
    GitVersionSettings buildServerSettings = new GitVersionSettings {
        OutputType = GitVersionOutput.BuildServer,
        UpdateAssemblyInfo = true
    };

    SetGitVersionPath(buildServerSettings);

    // Ran twice because the result is empty when using buildserver mode but we need to output to TeamCity
    // and use the result
    GitVersion(buildServerSettings);

    GitVersionSettings localSettings = new GitVersionSettings();

    SetGitVersionPath(localSettings);

    var versionResult = GitVersion(localSettings);

    Information("AssemblySemVer: " + versionResult.AssemblySemVer);

    // Convert 12.1.3.4 to 1201030004 etc.
    string paddedVersionNumber = string.Join("", versionResult.AssemblySemVer.Split('.').Select(s => s.PadLeft(2, '0')).ToArray()) + "00";

    Information("PaddedVersionNumber: " + paddedVersionNumber);

    BuildNumber = versionResult.SemVer;
    BranchName = versionResult.BranchName;

    Information("BuildNumber updated: " + BuildNumber);
});

Task("Install-AngularCLI")
    .IsDependentOn("Version")
    .Does(() => 
{
    if (ngPath != null && FileExists(ngPath))
    {
        Information("Found Angular CLI at {0}.", ngPath);
        return;
    }

    DirectoryPath ngDirectoryPath = MakeAbsolute(Directory("./Tools/ng"));

    EnsureDirectoryExists(ngDirectoryPath);

    Cmd(npmPath,
        new ProcessArgumentBuilder()
                .Append("install")
                .Append("--prefix")
                .AppendQuoted(ngDirectoryPath.FullPath)
                .Append("@angular/cli")
    );

    ngPath = Context.Tools.Resolve("ng.cmd");
});

Task("Clean-Angular-App")
    .IsDependentOn("Install-AngularCLI")
    .Does( ()=> 
{
    CleanDirectory("./HHAExchange.MultiPayer.App/dist");
});

Task("Build-Angular-App")
    .IsDependentOn("Clean-Angular-App")
    .Does(() =>
{
    var npmInstallSettings = new NpmInstallSettings 
    {
      WorkingDirectory = "./HHAExchange.MultiPayer.App",
      LogLevel = NpmLogLevel.Warn,
      ArgumentCustomization = args => args.Append("--no-save")
    };

    NpmInstall(npmInstallSettings);

    var runSettings = new NpmRunScriptSettings 
    {
      ScriptName = "ng",
      WorkingDirectory = "./HHAExchange.MultiPayer.App",
      LogLevel = NpmLogLevel.Warn
    };

    runSettings.Arguments.Add("build");
    runSettings.Arguments.Add("--prod");
    runSettings.Arguments.Add("--build-optimizer");
    runSettings.Arguments.Add("--progress false");

    NpmRunScript(runSettings);

    CopyFiles("./HHAExchange.MultiPayer.App/web.config", "./HHAExchange.MultiPayer.App/dist");
});

Task("RestorePackages")
    .Does(() =>
{
    NuGetRestore("HHAExchange.MultiPayer.sln");

    DotNetCoreRestore("HHAExchange.MultiPayer.sln");
});

Task("Build-API")
    .IsDependentOn("Version")
    .IsDependentOn("RestorePackages")
    .Does(() =>
{
    var msBuildSettings = new MSBuildSettings()
        .SetConfiguration("Release")
        .WithProperty("DeployOnBuild", "true");

    if(!string.IsNullOrEmpty(MsBuildLogger))
    {
        msBuildSettings.ArgumentCustomization = arguments =>
                arguments.Append(string.Format("/logger:{0}", MsBuildLogger));
    }
        
    MSBuild("HHAExchange.MultiPayer.sln", msBuildSettings);
});

Task("Test-API")
    .IsDependentOn("Build-API")
    .Does(() =>
{
    var settings = new DotNetCoreTestSettings()
    {
        NoBuild = true,
        Configuration = "Release"
    };

    var testAdapterPath = GetFiles("./**/vstest15/TeamCity.VSTest.TestAdapter.dll").First();

    Information("Test Adapter Path " + testAdapterPath);

    if (TeamCity.IsRunningOnTeamCity) 
    {
        settings.Logger = "teamcity";
        settings.TestAdapterPath = testAdapterPath.GetDirectory();
    }

	var projectFiles = GetFiles("*/*Tests.csproj");

    foreach(var file in projectFiles)
    {
        DotNetCoreTest(file.FullPath, settings);
    }
});

Task("Test-Angular-App")
    .IsDependentOn("Build-Angular-App")
    .Does(() => 
    {
        var runSettings = new NpmRunScriptSettings 
        {
            ScriptName = "ng",
            WorkingDirectory = "./HHAExchange.MultiPayer.App",
            LogLevel = NpmLogLevel.Warn
        };

        runSettings.Arguments.Add("test");
        runSettings.Arguments.Add("--karmaConfig=karma.conf.js");
        runSettings.Arguments.Add("--watch=false --browsers=ChromeHeadless");

        NpmRunScript(runSettings);
    });

Task("Test")
    .IsDependentOn("Test-Angular-App")
    .IsDependentOn("Test-API");

Task("Pack-API")
    .Does(() => 
{
    DeleteFiles("./publishpackage/HHAExchange.MultiPayer.API.*");

    string publishDirectory = "./HHAExchange.MultiPayer.API/bin/publish";

    var publishSettings = new DotNetCorePublishSettings
    {
        Configuration = "Release",
        OutputDirectory = publishDirectory
    };

    DotNetCorePublish("HHAExchange.MultiPayer.API/HHAExchange.MultiPayer.API.csproj", publishSettings);

    var nuGetPackSettings = new NuGetPackSettings 
    {
        OutputDirectory = "./publishpackage/",
        BasePath = "./HHAExchange.MultiPayer.API/bin/publish/",
        Version = BuildNumber
    };

    NuGetPack("./HHAExchange.MultiPayer.API/HHAExchange.MultiPayer.API.nuspec", nuGetPackSettings); 
});

Task("Pack-Angular-App")
    .Does(() => 
{
    CleanDirectory("./HHAExchange.MultiPayer.App/bin/publish");

    DeleteFiles("./publishpackage/HHAExchange.MultiPayer.App.*");
    
	CopyDirectory("./HHAExchange.MultiPayer.App/dist", "./HHAExchange.MultiPayer.App/bin/publish");
	
	DeleteDirectory("./HHAExchange.MultiPayer.App/dist", new DeleteDirectorySettings 
	{
		Recursive = true,
		Force = true
	});	
    
    var nuGetPackSettings = new NuGetPackSettings 
    {
        OutputDirectory = "./publishpackage/",
        BasePath= "./HHAExchange.MultiPayer.App/bin/publish/",
        Version = BuildNumber
    };
		
    NuGetPack("./HHAExchange.MultiPayer.App/HHAExchange.MultiPayer.App.nuspec", nuGetPackSettings);
});

Task("Pack")
    .IsDependentOn("Test")
    .IsDependentOn("Pack-API")
    .IsDependentOn("Pack-Angular-App");

Task("OctoPush")
  .IsDependentOn("Pack")
  .Does(() => 
{
    if (BuildNumber.Contains("-develop") || BuildNumber.Contains("-release") || IsFeatureBranchWithTenant())
    {
        Information("Push packages to Octopus");

        OctoPush(OctopusDeployUrl, 
                    OctopusDeployApiKey, 
                    GetFiles("./publishpackage/*.*"),
                    new OctopusPushSettings 
                    {
                        ReplaceExisting = true
                    });
    }
});

Task("OctoRelease")
  .IsDependentOn("OctoPush") 
  .Does(() => 
{
	var releaseSettings = new CreateReleaseSettings 
	{
		ApiKey = OctopusDeployApiKey,
		ArgumentCustomization = args => args.Append("--packageVersion " + BuildNumber),
        ReleaseNumber = BuildNumber,
		Server = OctopusDeployUrl
    };

	if (BuildNumber.Contains("-develop")) 
	{
		releaseSettings.Channel = "Trunk";
		releaseSettings.DeployTo = "DEV";
	} 
	else if (BuildNumber.Contains("-release"))
	{
		releaseSettings.Channel = "Release";
	}
	else if(!string.IsNullOrEmpty(tenant))
	{
		releaseSettings.Channel = "Feature";
		releaseSettings.Tenant = new string[]{tenant};
		releaseSettings.DeployTo = "DEV";
	}

    if(!string.IsNullOrEmpty(releaseSettings.Channel))
	{
		Information("Deploying to target project: "+ ProjectName);

		OctoCreateRelease(ProjectName, releaseSettings);
	}
	else
	{
		Information("Deployment is not enabled for this branch");
	}
});

public void SetGitVersionPath(GitVersionSettings settings)
{
    if (TeamCity.IsRunningOnTeamCity)
    {
        Information("Using shared GitVersion");

        settings.ToolPath = "c:\\tools\\gitversion\\gitversion.exe";
    }
}

private bool IsFeatureBranchWithTenant()
{
    Information("Deployment Branches are: "+DeploymentBranches);
    Information("Current Branch is: "+BranchName);

    if(!string.IsNullOrEmpty(DeploymentBranches))
    {
        var deploymentBranches = DeploymentBranches.Split(new[]{ ',' }, System.StringSplitOptions.RemoveEmptyEntries);
        
        foreach(string deploymentBranch in deploymentBranches)
        {
            Information("Checking if branch is:" + deploymentBranch);

            if(BranchName.ToLower() == deploymentBranch.Trim().ToLower())
            {
                var pattern = "([^/]*)([/]*)([^-_]+(?:-|_)[^-_]+)([-|_])(.*)";
        
                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                var match = r.Match(BranchName);

                if (match.Success)
                {
                    var currentBranchFolder = match.Groups[1].Value; 
                    var currentTicketNumber = match.Groups[3].Value;
                    var currentBranch = match.Groups[5].Value;

                    Information("Folder: " + currentBranchFolder);
                    Information("Ticket: " + currentTicketNumber);
                    Information("Branch: " + currentBranch);

                    tenant = currentTicketNumber;

                    Information("Using tenant:" + tenant);

                    return true;
                }
            }
        }

        return false;
    }

    return false;
}

RunTarget(Target);