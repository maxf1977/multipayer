﻿using HHAExchange.MultiPayer.Application.Payer;
using HHAExchange.Payer.Infrastructure;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HHAExchange.MultiPayer.Core.Payer
{
    public class GetAgencyKeywordsRequestHandler : IRequestHandler<GetAgencyKeywordsRequestModel, GetAgencyKeywordsResponseModel>
    {
        private readonly HhaContext _context;

        public GetAgencyKeywordsRequestHandler(HhaContext context) => _context = context;

        public async Task<GetAgencyKeywordsResponseModel> Handle(GetAgencyKeywordsRequestModel request, CancellationToken cancellationToken) 
            => await Task.FromResult(new GetAgencyKeywordsResponseModel()
                   {
                       Keywords = _context.AgencyKeywords.Take(request.ItemCount).Select(ak => ak.Keyword).ToList()
                   });
    }
}
