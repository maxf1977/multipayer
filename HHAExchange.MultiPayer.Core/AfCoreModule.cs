﻿using Autofac;
using MediatR.Extensions.Autofac.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace HHAExchange.Multipayer.Infrastructure
{
    public class AfCoreModule : Module
    {
        private readonly IConfiguration _configuration;

        public AfCoreModule(IConfiguration configuration) => _configuration = configuration;

        protected override void Load(ContainerBuilder builder)
        {
            builder.AddMediatR(GetType().Assembly);
            base.Load(builder);
        }
    }
}
