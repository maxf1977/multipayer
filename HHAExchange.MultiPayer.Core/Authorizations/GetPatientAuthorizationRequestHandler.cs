﻿using HHAExchange.MultiPayer.Domain.Entities.Authorization;
using HHAExchange.MultiPayer.Application.Authorizations;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace HHAExchange.MultiPayer.Core.Authorizations
{
    public class GetPatientAuthorizationRequestHandler: IRequestHandler<GetPatientAuthorizationsRequestModel, List<PatientAuthorization>>
    {
        public Task<List<PatientAuthorization>> Handle(GetPatientAuthorizationsRequestModel request, CancellationToken cancellationToken)
            => new Task<List<PatientAuthorization>>(request => new List<PatientAuthorization>(), request);
    }
}
