﻿using Autofac;
using Microsoft.Extensions.Configuration;

namespace HHAExchange.Extensions.Base.Ioc
{
    public abstract class AfModuleBase : Module
    {
        public AfModuleBase(IConfiguration configuration) => Configuration = configuration;

        protected IConfiguration Configuration { get; }
    }
}
