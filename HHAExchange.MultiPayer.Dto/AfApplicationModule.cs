﻿using Autofac;
using MediatR.Extensions.Autofac.DependencyInjection;

namespace HHAExchange.Multipayer.Infrastructure
{
    public class AfApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.AddMediatR(GetType().Assembly);
            base.Load(builder);
        }
    }
}
