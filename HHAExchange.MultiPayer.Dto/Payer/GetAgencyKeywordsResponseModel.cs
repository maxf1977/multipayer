﻿using MediatR;
using System.Collections.Generic;

namespace HHAExchange.MultiPayer.Application.Payer
{
    public class GetAgencyKeywordsResponseModel
    {
        public List<string> Keywords { get; set; }
    }
}
