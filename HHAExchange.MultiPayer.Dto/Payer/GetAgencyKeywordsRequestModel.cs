﻿using MediatR;

namespace HHAExchange.MultiPayer.Application.Payer
{
    public class GetAgencyKeywordsRequestModel : IRequest<GetAgencyKeywordsResponseModel>
    {
        public int ItemCount { get; set; }
    }
}
