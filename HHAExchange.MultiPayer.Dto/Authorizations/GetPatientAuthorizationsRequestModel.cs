﻿using System.Collections.Generic;

using HHAExchange.MultiPayer.Domain.Entities.Authorization;
using MediatR;

namespace HHAExchange.MultiPayer.Application.Authorizations
{
    public class GetPatientAuthorizationsRequestModel : IRequest<List<PatientAuthorization>>
    {
    }
}
